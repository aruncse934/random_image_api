# Random_Image_API

Random image API
Problem statement

Build a pseudo Backend API that displays a random image from the internet.

Minimum requirement

Write a backend in the provided tech stack that exposes an API that returns the necessary response.
When the base URL